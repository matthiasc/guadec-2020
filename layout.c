#include <gtk/gtk.h>

G_DECLARE_FINAL_TYPE (DemoWidget, demo_widget, DEMO, WIDGET, GtkWidget)

struct _DemoWidget
{
  GtkWidget parent_instance;

  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
};

struct _DemoWidgetClass
{
  GtkWidgetClass parent_class;
};

G_DEFINE_TYPE (DemoWidget, demo_widget, GTK_TYPE_WIDGET)

static void
demo_widget_init (DemoWidget *demo)
{
  demo->label1 = gtk_label_new ("Red");
  gtk_widget_set_parent (demo->label1, GTK_WIDGET (demo));

  demo->label2 = gtk_label_new ("Green");
  gtk_widget_set_parent (demo->label2, GTK_WIDGET (demo));

  demo->label3 = gtk_label_new ("Blue");
  gtk_widget_set_parent (demo->label3, GTK_WIDGET (demo));

  demo->label4 = gtk_label_new ("Yellow");
  gtk_widget_set_parent (demo->label4, GTK_WIDGET (demo));
}

static void
demo_widget_dispose (GObject *object)
{
  DemoWidget *demo = DEMO_WIDGET (object);

  g_clear_pointer (&demo->label1, gtk_widget_unparent);
  g_clear_pointer (&demo->label2, gtk_widget_unparent);
  g_clear_pointer (&demo->label3, gtk_widget_unparent);
  g_clear_pointer (&demo->label4, gtk_widget_unparent);

  G_OBJECT_CLASS (demo_widget_parent_class)->dispose (object);
}

static void
demo_widget_class_init (DemoWidgetClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->dispose = demo_widget_dispose;

  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_GRID_LAYOUT);
}

GtkWidget *
demo_widget_new (void)
{
  return g_object_new (demo_widget_get_type (), NULL);
}

int
main (int argc, char *argv[])
{
  GtkWindow *window;
  GtkWidget *demo;

  gtk_init ();

  window = GTK_WINDOW (gtk_window_new ());

  demo = demo_widget_new ();

  gtk_window_set_child (window, demo);

  gtk_window_present (window);

  while (g_list_model_get_n_items (gtk_window_get_toplevels ()) > 0)
    g_main_context_iteration (NULL, TRUE);

  return 0;
}
