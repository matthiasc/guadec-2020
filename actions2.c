#include <gtk/gtk.h>

G_DECLARE_FINAL_TYPE (DemoWidget, demo_widget, DEMO, WIDGET, GtkWidget)

struct _DemoWidget
{
  GtkWidget parent_instance;

  GtkWidget *label1;
  GtkWidget *label2;
};

struct _DemoWidgetClass
{
  GtkWidgetClass parent_class;
};

G_DEFINE_TYPE (DemoWidget, demo_widget, GTK_TYPE_WIDGET)

static void
red_clicked_cb (GtkGestureClick *gesture,
                int              n_pressed,
                double           x,
                double           y,
                DemoWidget      *demo)
{
  gtk_widget_activate_action (GTK_WIDGET (demo), "turn-red", NULL);
}

static void
blue_clicked_cb (GtkGestureClick *gesture,
                 int              n_pressed,
                 double           x,
                 double           y,
                 DemoWidget      *demo)
{
  gtk_widget_activate_action (GTK_WIDGET (demo), "turn-blue", NULL);
}

static void
turn_red (GtkWidget  *widget,
          const char *actionname,
          GVariant   *parameter)
{
  gtk_widget_remove_css_class (widget, "blue");
  gtk_widget_add_css_class (widget, "red");
}

static void
turn_blue (GtkWidget  *widget,
           const char *actionname,
           GVariant   *parameter)
{
  gtk_widget_remove_css_class (widget, "red");
  gtk_widget_add_css_class (widget, "blue");
}

static void
demo_widget_init (DemoWidget *demo)
{
  GtkEventController *controller;

  gtk_widget_set_focusable (GTK_WIDGET (demo), TRUE);

  demo->label1 = gtk_label_new ("Red");
  gtk_widget_set_hexpand (demo->label1, TRUE);
  gtk_widget_set_parent (demo->label1, GTK_WIDGET (demo));

  demo->label2 = gtk_label_new ("Blue");
  gtk_widget_set_hexpand (demo->label2, TRUE);
  gtk_widget_set_parent (demo->label2, GTK_WIDGET (demo));

  controller = GTK_EVENT_CONTROLLER (gtk_gesture_click_new ());
  g_signal_connect (controller, "pressed",
                    G_CALLBACK (red_clicked_cb), demo);
  gtk_widget_add_controller (demo->label1, controller);

  controller = GTK_EVENT_CONTROLLER (gtk_gesture_click_new ());
  g_signal_connect (controller, "pressed",
                    G_CALLBACK (blue_clicked_cb), demo);
  gtk_widget_add_controller (demo->label2, controller);
}

static void
demo_widget_dispose (GObject *object)
{
  DemoWidget *demo = DEMO_WIDGET (object);

  g_clear_pointer (&demo->label1, gtk_widget_unparent);
  g_clear_pointer (&demo->label2, gtk_widget_unparent);

  G_OBJECT_CLASS (demo_widget_parent_class)->dispose (object);
}

static void
demo_widget_class_init (DemoWidgetClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->dispose = demo_widget_dispose;

  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_BOX_LAYOUT);

  gtk_widget_class_install_action (widget_class, "turn-red", NULL, turn_red);
  gtk_widget_class_install_action (widget_class, "turn-blue", NULL, turn_blue);

  gtk_widget_class_add_binding_action (widget_class,
                                       GDK_KEY_r, GDK_CONTROL_MASK,
                                       "turn-red", NULL);
  gtk_widget_class_add_binding_action (widget_class,
                                       GDK_KEY_b, GDK_CONTROL_MASK,
                                       "turn-blue", NULL);
}

GtkWidget *
demo_widget_new (void)
{
  return g_object_new (demo_widget_get_type (), NULL);
}

int
main (int argc, char *argv[])
{
  GtkWindow *window;
  GtkWidget *demo;
  GtkCssProvider *provider;

  gtk_init ();

  window = GTK_WINDOW (gtk_window_new ());

  demo = demo_widget_new ();

  gtk_window_set_child (window, demo);

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_path (provider, "./demo.css");
  gtk_style_context_add_provider (gtk_widget_get_style_context (demo),
                                  GTK_STYLE_PROVIDER (provider),
                                  1000);

  gtk_window_present (window);

  while (g_list_model_get_n_items (gtk_window_get_toplevels ()) > 0)
    g_main_context_iteration (NULL, TRUE);

  return 0;
}
